class CreateJobs < ActiveRecord::Migration
  def up
    create_table :jobs do |t|
      t.column :to_learn, :text
      t.column :orientation_date, :date
      t.column :firstname, :text
      t.column :lastname, :text
      t.column :email, :text
      t.column :contact, :bigint
      t.column :message, :text
      t.timestamps null: false
    end
  end

  def down
  	drop_table :jobs
  end
end

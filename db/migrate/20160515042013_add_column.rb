class AddColumn < ActiveRecord::Migration
  def up
  	add_column :jobs, :status, :string, default: "pending"
  	add_column :assistants, :status, :string, default: "pending"
  end

  def down
  	remove_column :jobs, :status
  	remove_column :assistants, :status
  end
end

class CreateAssistants < ActiveRecord::Migration
  def up
    create_table :assistants do |t|
      t.column :help_me_with, :string
      t.column :available_at, :string
      t.column :firstname, :text
      t.column :lastname, :text
      t.column :company, :text
      t.column :email, :text
      t.column :contact, :bigint
      t.column :message, :text
      t.timestamps null: false
    end
  end

  def down
  	drop_table :assistants
  end
end

virtual
	.directive( "addData", [
		"Server",
		"Util",
		function directive ( Server, Util ) {
			return {
				"restrict": "A",
				"scope": true,				
				"link": function onLink ( scope , element , attributeSet ) {
				 	scope.assistant = { }; 
				 	scope.job = { };
				 	scope.message = { };
				 	scope.assistantDetails = { };
				 	scope.showneed = 0;
				 	scope.yes = "activated";
				 	scope.no = "";


				 	scope.setShowneed = function( data ){
				 		scope.showneed = data;
				 		if (data == 1){
				 			scope.no = "activated";
				 			scope.yes = "";
				 		}else{
				 			scope.no = "";
				 			scope.yes = "activated";
				 		}
				 	};


				 	scope.addAssistant = function ( ) {
				 		if ( !scope.assistant.available_at ) {
				 			scope.assistant.available_at = scope.assistant.other;				 			
				 		}
				 		delete scope.assistant.other;
				 		scope.message.email = scope.assistant.email;
				 		scope.message.name = scope.assistant.firstname+" "+scope.assistant.lastname;
				 		scope.message.body = "I need a Trained Virtual Staff, to help me with: "+scope.assistant.help_me_with+" , to be available: "+scope.assistant.available_at+" , Contact number: "+scope.assistant.contact+" , My company: "+scope.assistant.company+" , My Message: "+scope.assistant.message;
				 		Server.addAssistant( scope.assistant )
							.then( function ( response ) {
								scope.assistant = { };
								var modal = $( "#myModal" );
				 				modal.css( "display" , "none" );
				 				scope.sendMail();
							} );
				 		
				 		
				 	};


				 	scope.addJob = function (  ) {
				 		scope.job.orientation_date = scope.job.orientation_date.toDateString();
				 		scope.message.email = scope.job.email;
				 		scope.message.name = scope.job.firstname+" "+scope.job.lastname;
				 		scope.message.body = "I need a Job, I Want to learn: "+scope.job.to_learn+" , Orientation_date: "+scope.job.orientation_date+" , Contact number: "+scope.job.contact+" , My Message: "+scope.job.message;

				 		Server.addJob( scope.job )
							.then( function ( response ) {
								scope.job = { };
								var modal = $( "#myModal2" );
						 		modal.css( "display" , "none" );
						 		var modal = $( "#myModal" );
						 		modal.css( "display" , "none" );
						 		scope.sendMail();
							} );
						
						
				 	};


				 	scope.weekEnds = function ( date ) {
				 		var day = date.getDay( );
				 		return (day == 0 || day == 6);
				 	};
				 	
				 	scope.sendMail = function (  ) {
				 		$('#spinner').show();
				 		Server.sendNow( scope.message )
							.then( function ( response ) {
								$('#spinner').hide();
								scope.message = { };
								Util.showAlert( "check.png", "Message", "Message Successfully Sent!\n Just constantly check your email for replies." ,"error");
							} );			
				 	};

				 	scope.sendReply = function ( email, name ,id, type ) {
				 		$('#spinner').show();
				 		$('#spinner2').show();
				 		scope.message.email = email;
				 		scope.message.name = name;
				 		Server.sendReply( scope.message )
							.then( function ( response ) {
								$('#spinner').hide();
				 				$('#spinner2').hide();
								scope.message = { };
								if (type == 'job') {
									Server.jobStatus( id ).then( function ( response ) {  } );
								}else{
									Server.assistantStatus( id ).then( function ( response ) { } );
								}

								Util.showAlert( "check.png", "Message", "Message Successfully Sent!\n Just constantly check your email for replies." ,"error");

							} );			
				 	};

				 	scope.getSunday = function (  ) {
				 		var curr = new Date; // get current date
						var first = curr.getDate() - curr.getDay();
						var last = first+6;
						return new Date(curr.setDate(last));
				 	};

				 	scope.select = function select ( number ) {
						scope.count = number;
					};

					scope.openDetails = function openDetails( id , type, view){

						Server.showDetails( id, type, view )
							.then( function ( response ) {
								
								scope.assistantDetails = response.data.assistant;
								console.log(scope.assistantDetails);
								var modal = $( view );
				 		        modal.css( "display" , "block" );
							} );
					};

					scope.delete = function ( id, type ) {
								if (type == 'job') {
									Server.deleteJob( id ).then( function ( response ) { 
										alert("Successfully deleted");
											location.reload(true);
										
										
									  } );
								}else{
									Server.deleteAssistant( id ).then( function ( response ) { 
										alert("Successfully deleted");
										scope.close();
										
									} );
								}
					};

					scope.close = function ( ) {
						location.reload(true);
					};




					scope.count = 1;
				 	scope.job.orientation_date = scope.getSunday();
				}
			}
		}
	] );
virtual
	.factory( "Server" , [
		"$http",
		function factory ( $http ) {
			var server = {
				"request": function restApi ( method , path , params ) {
					return $http[ method ]( path , ( params || { } ) );
				},
				"addAssistant": function addAssistant ( assistant ) {
					return server.request( "post", "/api/new/assistant", {"assistant" : assistant} );
				},
				"addJob": function addJob ( job ) {
					return server.request( "post", "/api/add/job", {"job" : job} );
				}, 
				"showDetails": function showDetails ( id, type ) {
					return server.request( "post", "/api/get/"+type, {"id" : id} );
				},
				"jobStatus": function jobStatus ( id ) {
					return server.request( "post", "/api/update/job/status", {"id" : id} );
				},
				"assistantStatus": function assistantStatus ( id ) {
					return server.request( "post", "/api/update/assi/status", {"id" : id} );
				},
				"deleteJob": function deleteJob ( id ) {
					return server.request( "post", "/api/delete/job", {"id" : id} );
				},
				"deleteAssistant": function deleteAssistant ( id ) {
					return server.request( "post", "/api/delete/assistant", {"id" : id} );
				},
				"sendNow": function sendNow ( message ) {
					return server.request( "post", "/api/send/email", {"message" : message} );
				},
				"sendReply": function sendReply ( message ) {
					return server.request( "post", "/api/send/reply", {"message" : message} );
				}
			};

			return server;
		}
	] );
class VirtualahanMailer < ApplicationMailer
	default from: 'virtualahan@gmail.com'

  def send_notif(message)
    @message = message
    @url  = 'http://example.com/login'
    mail(to: 'virtualahan@gmail.com' ,reply_to: @message[:email], subject: "Virtualahan Have an Email.")
  end


  def send_reply(message)
    @message = message
    @email = @message[:email]
    @url  = 'http://example.com/login'
    mail(to: @email , subject: "You have an Email from Virtualahan")
  end

end

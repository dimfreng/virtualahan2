class ApiController < ApplicationController
	
	def send_email
		message = params[:message]
		VirtualahanMailer.send_notif(message).deliver_now
		 render json: {:status => "success"}, status: :created
	end

	def send_reply
		message = params[:message]
		VirtualahanMailer.send_reply(message).deliver_now
		 render json: {:status => "success"}, status: :created
	end

#assistant
	def create_assistant
		if Assistant.create(assistant_params)
			@message = "succesfully created."
		else
			@message = "Failed to create Assistant."
		end

		render json:{message: @message}
	end

	def get_all_assistant
		render json:{assistant: Assistant.all}
	end

	def update_assistant
		assistant = Assistant.find(params[:id])
		if assistant.update_attributes(assistant_params)
			@message = "succesfully updated"
		else
			@message = "Failed to update assistant"
		end

		render json:{message: @message}
	end

	def update_assistant_status
		assistant = Assistant.find(params[:id])
		assistant.status = "replied"
		assistant.save
		render json:{assistant: assistant}
	end

	def delete_assistant
		
		if Assistant.destroy(params[:id])
			@message = "succesfully deleted"
		else
			@message = "Failed to delete assistant"
		end
			render json:{message: @message}
	end

	def get_specific_assistant
		assistant = Assistant.find(params[:id])
		render json:{assistant: assistant}
	end

#job
	def create_job
		if Job.create(job_params)
			@message = "succesfully created."
		else
			@message = "Failed to create job."
		end

		render json:{message: @message}
	end

	def delete_job
		
		if Job.destroy(params[:id])
			@message = "succesfully deleted"
		else
			@message = "Failed to delete job"
		end
			render json:{message: @message}
	end

	def get_all_job
		render json:{job: Job.all}
	end

	def get_specific_job
		assistant = Job.find(params[:id])
		render json:{assistant: assistant}
	end

	def update_job_status
		job = Job.find(params[:id])
		job.status = "replied"
		job.save
		render json:{job: job}
	end

private

	def assistant_params
		params.require(:assistant).permit(:help_me_with, :available_at, :firstname, :lastname, :company, :email, :contact, :message)
	end

	def job_params
		params.require(:job).permit(:to_learn, :orientation_date, :firstname, :lastname, :email, :contact, :message)
	end


end

class AdminController < ApplicationController

	def admin
		if session[:accout]
			redirect_to action: "dashboard"
		end
	end

	def logout
		session[:accout] = false
		redirect_to action: "admin"
	end

	def dashboard
		if !session[:accout]
			redirect_to action: "admin"
		end
		@assistant = Assistant.all.order("status ASC").order("created_at DESC")
		@job = Job.all.order("status ASC").order("orientation_date ASC")
	
	end

	def set_session
		if params[:username] == "Virtualahan" && params[:password] == "VirTu@l@h@n"
			session[:accout] = true
			redirect_to :action => "dashboard" 
		else
			redirect_to :action => "admin"
		end
	end

end
